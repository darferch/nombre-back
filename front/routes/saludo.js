var express = require('express');
var router = express.Router();

router.get('/:name', function (req, res, next) {
    let name = req.params.name;
    res.send({msg:'Hola ' + name + " !!"});
});

module.exports = router;
