import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../environments/environment';


@Injectable({ providedIn: 'root' })
export class SaludoService {

    url = environment.baseUrl + '/saludo/';

    constructor(private http: HttpClient) { }

    saludar(nombre: string): Observable<string> {
        return this.http.get<Msg>(this.url + nombre).pipe(
            map(x => x.msg)
        );
    }
}


export class Msg {
    msg: string;
}
